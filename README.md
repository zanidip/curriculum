<img src="./pics/name.svg"   />

 - **Email**: clement.nolleau@gmail.com
 - **Linkedin**: https://www.linkedin.com/in/clement-nolleau/
 - **Gitlab**: https://gitlab.com/zanidip/ 
 - 35 Years old - Holding a **Working visa** in Hong Kong


# <img src="./pics/code.svg" width="28" height="28" />&nbsp; Summary

 - **Agile** enthusiast.
 - Inclined to own and take responsibilities on **complex technical solutions**.
 - **14 years** experience on critical infrastructures.
 - High capacity to absorb new workflows and technologies.
 - Adaptability and positivity in team work.
 - Technical writing : Architecture file, technical and functional procedure, documentation.

 
# <img src="./pics/brain.svg" width="28" height="28" />&nbsp; Core skills

 - Infrastructure : **AWS**, **Kubernetes**, Docker Swarm
 - System : **Linux** (Ubuntu, Debian, Centos), Mac
 - Continuous Delivery  : **terraform**, **ansible**, packer, Vagrant
 - Continuous Integration  : **Git**, gitlab-ci, circleci, Teamcity, Docker
 - Programming languages : **Python**, **Bash**, Ruby
 - Supervision and alerting  : Prometheus, Splunk, Graphana, Influx, Fluentd
 - Network security : **Juniper**, Fortinet, Cisco


# <img src="./pics/briefcase.svg" width="28" height="28" />&nbsp; Professional Experience


### &ensp;&ensp;&ensp;&ensp; <img src="./pics/briefcase.svg" width="20" height="20" /> CI / CD / Infrastructure engineer @ Crypto.com
_March 2020 - now_

_Confidential_

### &ensp;&ensp;&ensp;&ensp; <img src="./pics/briefcase.svg" width="20" height="20" /> Continuous Delivery Engineer @ OSL
_Sept 2018 - Feb 2020_

In a **highly critical Crypto trading platform**, creating in a team of 4 a complete Continuous integration / delivery solution :
```
- Deployment of services in one click in 3 reproductibles environments
- Building of services and deployment to testing / prod environment 100% automated
- 100+ ansible roles, 50+ terraform modules
- Integration of end to end automated testing in stagging environments
- Tooling : AWS - Terraform - Ansible - Teamcity - docker - Gitlab - Python - linux - packer - vault
```

<div class="page"/>

Leading the evolution of the infrastructure to **container orchestration** :
```
- Creation of POC with the 3 leader technologies ( Kubernetes / Amazon ECS /  Docker swarm )
- Creation of continuous delivery pipeline based on gitlab-ci in a mix of selfhosted/cloud environment
- Work with developers and infrastructure team to make choices
```

### &ensp;&ensp;&ensp;&ensp; <img src="./pics/briefcase.svg" width="20" height="20" /> Expertise in technical infrastructure of the SI @ NetXP
_May 2017 - Jan 2018_

**In an international bank** : Integration and security expertise
Within the entity responsible for the evolution of internal clients of the bank security solutions:
Cisco routers, Juniper, checkpoint, Fortinet firewalls, splunk, ansible, LAMP
```
- Studies of technical solutions,
- Project management,
- Operational procedures and documentation writing,
- Advice and expertise with the production teams.
```


**In a a leader in the jewelry store in Europe** : Audit of network and security infrastructure
The management wanted to perform an audit of the entire infrastructure and network security (Headquarters, data-center, shops)
```
- Initialization of the scope of the audit,
- Data collection through interviews with the local IT,
- Audit of infrastructure. Identification of gaps with the State of the art,
- Final report writing with analysis and recommendations of evolutions.
```


### &ensp;&ensp;&ensp;&ensp; <img src="./pics/briefcase.svg" width="20" height="20" /> Network and security engineer @ INSEEC
_Oct 2009 - Jan 2017_

The INSEEC group is a group of management & communication grad schools present in 20 sites in 4 countries. The network infrastructure includes 30,000 users (200 active network assets and 10 servers).
Perimeter under my responsibility: Network distribution, Wi-Fi, firewalls, VPNs, Linux infrastructure. _(Cisco, Juniper, Aruba, Ipsec/ssl VPN, Python, Bash, Ansible)._
 
**Maintenance in operational conditions of the infrastructure**
```
- Documents of architecture and operational writing, reporting, L3 support,
- Technological and management upgrades, Supervision and alerting,
- Management of relationships with internet operators,
```

**Integration of new technological solutions**
```
- Analysis of the actual infrastructure and the State of the art,
- Design, preparation and integration.
``` 
**IT risks management**
```
- Technological vulnerability and security analysis,
- Internal and external penetration testing,
- Management and analysis of logs (SIEM),
- Forensic analysis and corrective measures.
```
<div class="page"/>

**Development of an internal network probe** based on open source technology
The product we build was 90% less expensive that customer products.
```
- Studies of the requirement, development of the probe based on embedded linux and open source software
- Tests and automation
```

# <img src="./pics/grade.svg" width="28" height="28" />&nbsp; Education

### &ensp;&ensp;&ensp;&ensp; <img src="./pics/grade.svg" width="20" height="20" /> Engineering School (Master degree) @ EIGSI
_2004 - 2009_

5 years curriculum. Specialty "Networks and Telecom".
```
- theoretical and practical courses on computer networks (TCP/IP, principles and technologies of networks, System admin).
- Student projects: Modems, RF design, artificial intelligence and genetic algorithm, robotics, creation and animation of a 2D and 3D computer graphics club.
```


### &ensp;&ensp;&ensp;&ensp; <img src="./pics/grade.svg" width="20" height="20" /> Internships


**Network and security** @ NES _(6 months, 2009)_

- Daily participation in the support, development, support on networks and security technologies. Pentests, developments of business focused security tools.

**Software development** @ Telecom Design _(3 months, 2008)_

- Designing the interface of a mobile and multi service router. UI design, embedded Linux, Python & TCL.

**Tools development**  @ Apex Computer _(3 months, 2006)_

- Development of tools for windows diagnostic. C++, Java


# <img src="./pics/hobby.svg" width="28" height="28" />&nbsp; Hobbies

- **Fablab** : electronic projects, RF, prototyping, CAD, Embedded Linux 
- **Roller derby** (collective sport) : Creation of a sport club in Paris, practice at international level.
